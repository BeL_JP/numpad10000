﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using NP10000;

namespace IRTransceiverApp
{
    public partial class IRTForm : Form
    {
		private ArduinoTransceiver arduinoTransceiver;
        public IRTForm()
        {
            arduinoTransceiver = new ArduinoTransceiver();
            arduinoTransceiver.IRReceived += Irt_IRReceived;
            InitializeComponent();
            string[] files = Directory.GetFiles(Program.ProfileDir, "*.irb");
            foreach(var file in files)
            {
                profileListBox.Items.Add(Path.GetFileName(file));
            }
        }

        private void Irt_IRReceived(object sender, EventArgs e)
        {
            arduinoTransceiver.SetIRTransmitterMode();
            Invoke((MethodInvoker)delegate {
                receiveTimer.Stop();
                newProfileTextBox.Text = "";
                receivedPanel.Visible = true;
            });
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            string selected;
            if (profileListBox.SelectedItem != null)
            {
                selected = profileListBox.SelectedItem.ToString();
                arduinoTransceiver.CastIR(Path.Combine(Program.ProfileDir, selected));
            }
            
        }

        private void receiveButton_Click(object sender, EventArgs e)
        {
            sendButton.Enabled = false;
            receiveButton.Text = "scanning...";
            arduinoTransceiver.SetIRReceiverMode();
            receiveTimer.Start();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (newProfileTextBox.Text == "")
                return;

            //ファイルを作成して書き込む
            //ファイルが存在しているときは、上書きする
            System.IO.FileStream fs = new System.IO.FileStream(Path.Combine(
                Program.ProfileDir, newProfileTextBox.Text + ".irb"),
                System.IO.FileMode.Create,
                System.IO.FileAccess.Write);
            //バイト型配列の内容をすべて書き込む
            fs.Write(arduinoTransceiver.LastReceivedData, 0, arduinoTransceiver.LastReceivedData.Length);
            //閉じる
            fs.Close();
            profileListBox.Items.Add(newProfileTextBox.Text + ".irb");
            receivedPanel.Visible = false;
            receiveButton.Text = "Receive";
            sendButton.Enabled = true;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            receivedPanel.Visible = false;
            receiveButton.Text = "Receive";
            sendButton.Enabled = true;
        }

        private void receiveTimer_Tick(object sender, EventArgs e)
        {
            arduinoTransceiver.SetIRTransmitterMode();
            receiveButton.Text = "Receive";
            sendButton.Enabled = true;
            receiveTimer.Stop();
        }
    }
}
