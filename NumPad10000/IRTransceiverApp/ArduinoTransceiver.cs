﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace NP10000
{
    public class ArduinoTransceiver : IDisposable
    {
        public event EventHandler IRReceived;
        public event EventHandler MoveDetected;
        public event EventHandler MoveUndetected;
        public byte[] LastReceivedData;

        private enum COMMAND { CHMODE_SEND = 0x81, CHMODE_RCV = 0x82, CAST_IR = 0xF1, CATCH_IR = 0xF2, HANDSHAKE = 0x8F, HANDSHAKE_RES = 0xF8, MOVE_DET = 0xE1, MOVE_UNDET = 0xE2, READY = 0xFF, RESUME = 0x11 }
        private Thread threadObject;
        private SerialPort serialPort;
        private bool closeListener = false;
        private List<string> castIRRequests = new List<string>();
        public ArduinoTransceiver()
        {
            threadObject = new Thread(new ThreadStart(Listener));
            string[] ports = SerialPort.GetPortNames();
            foreach(var p in ports)
            {
                byte[] buf = new byte[1];
                serialPort = new SerialPort();
                serialPort.PortName = p;
                serialPort.BaudRate = 9600;
                serialPort.DataBits = 8;
                serialPort.Parity = Parity.None;
                serialPort.StopBits = StopBits.One;
                try
                {
                    serialPort.Open();
                }
                catch
                {
                    serialPort.Close();
                    continue;
                }
                byte[] b = new byte[1];
                b[0] = (byte)COMMAND.RESUME;
                Thread.Sleep(3000);
                serialPort.Write(b, 0, 1);
                Thread.Sleep(500);
                serialPort.DiscardInBuffer();
                b[0] = (byte)COMMAND.HANDSHAKE;
                serialPort.Write(b, 0, 1);
                serialPort.ReadTimeout = 500;
                try
                {
                    serialPort.Read(buf, 0, 1);
                }
                catch
                {
                    serialPort.Close();
                    continue;
                }
                if (buf.Length == 0 || buf[0] != (byte)COMMAND.HANDSHAKE_RES)
                {
                    serialPort.Close();
                    continue;
                }
                break;
            }
            if(serialPort.IsOpen){
                SetIRTransmitterMode();
                threadObject.Start();
            }
            else
                throw new Exception();
        }

        public void Dispose()
        {
            closeListener = true;
            Thread.Sleep(1000);
            if(threadObject.IsAlive)
                threadObject.Abort();
            serialPort.Close();
        }

        private void Listener()
        {
            while (!closeListener)
            {
                if (serialPort.BytesToRead >= 1)
                {
                    byte[] buf = new byte[1];
                    serialPort.Read(buf, 0, 1);
                    if (buf[0] == (byte)COMMAND.CATCH_IR)
                    {
                        byte[] b = new byte[1];
                        byte[] buf2 = new byte[255];
                        b[0] = (byte)COMMAND.READY;
                        serialPort.Write(b, 0, 1);
                        serialPort.Read(buf2, 0, 255);
                        if (buf2[0] != (byte)COMMAND.CATCH_IR)
                            return;
                        buf2[0] = (byte)COMMAND.CAST_IR;
                        LastReceivedData = buf2;
                        if (IRReceived != null)
                            IRReceived(buf2, EventArgs.Empty);
                    }
                    else if (buf[0] == (byte)COMMAND.MOVE_DET)
                    {
                        if (MoveDetected != null)
                            MoveDetected(null, EventArgs.Empty);
                    }
                    else if(buf[0] == (byte)COMMAND.MOVE_UNDET)
                    {
                        if (MoveUndetected != null)
                            MoveUndetected(null, EventArgs.Empty);
                    }
                }
                if(castIRRequests.Count != 0)
                {
                    _CastIR(castIRRequests[0]);
                    castIRRequests.RemoveAt(0);
                }
                Thread.Sleep(50);
            }
        }

        public void CastIR(string filename)
        {
            castIRRequests.Add(filename);
        }

        public void CancelCastIR(string filename)
        {
            castIRRequests.Remove(filename);
        }

        private void _CastIR(string filename)
        {
            //ファイルを開く
            System.IO.FileStream fs = new System.IO.FileStream(
                filename,
                System.IO.FileMode.Open,
                System.IO.FileAccess.Read);
            //ファイルを読み込むバイト型配列を作成する
            byte[] bs = new byte[fs.Length];
            //ファイルの内容をすべて読み込む
            fs.Read(bs, 0, bs.Length);
            //閉じる
            fs.Close();
            byte[] b = new byte[1];
            b[0] = (byte)COMMAND.CAST_IR;
            serialPort.Write(b, 0, 1);
            byte[] buf = new byte[1];
            try
            {
                serialPort.Read(buf, 0, 1);
            }
            catch
            {
                return;
            }
            if(buf[0]==(byte)COMMAND.READY)
               serialPort.Write(bs, 0, 255);
            System.Threading.Thread.Sleep(500);
        }

        public void SetIRReceiverMode()
        {
            byte[] b = new byte[1];
            b[0] = (byte)COMMAND.CHMODE_RCV;
            serialPort.Write(b, 0, 1);
        }

        public void SetIRTransmitterMode()
        {
            byte[] b = new byte[1];
            b[0] = (byte)COMMAND.CHMODE_SEND;
            serialPort.Write(b, 0, 1);
        }
    }
}
