﻿namespace IRTransceiverApp
{
    partial class IRTForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.profileListBox = new System.Windows.Forms.ListBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.receiveButton = new System.Windows.Forms.Button();
            this.newProfileTextBox = new System.Windows.Forms.TextBox();
            this.receivedPanel = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.receiveTimer = new System.Windows.Forms.Timer(this.components);
            this.receivedPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // profileListBox
            // 
            this.profileListBox.FormattingEnabled = true;
            this.profileListBox.ItemHeight = 12;
            this.profileListBox.Location = new System.Drawing.Point(9, 10);
            this.profileListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.profileListBox.Name = "profileListBox";
            this.profileListBox.Size = new System.Drawing.Size(253, 340);
            this.profileListBox.TabIndex = 0;
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(204, 354);
            this.sendButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(57, 21);
            this.sendButton.TabIndex = 1;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // receiveButton
            // 
            this.receiveButton.Location = new System.Drawing.Point(9, 356);
            this.receiveButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.receiveButton.Name = "receiveButton";
            this.receiveButton.Size = new System.Drawing.Size(80, 18);
            this.receiveButton.TabIndex = 2;
            this.receiveButton.Text = "Receive";
            this.receiveButton.UseVisualStyleBackColor = true;
            this.receiveButton.Click += new System.EventHandler(this.receiveButton_Click);
            // 
            // newProfileTextBox
            // 
            this.newProfileTextBox.Location = new System.Drawing.Point(2, 2);
            this.newProfileTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.newProfileTextBox.Name = "newProfileTextBox";
            this.newProfileTextBox.Size = new System.Drawing.Size(136, 19);
            this.newProfileTextBox.TabIndex = 3;
            // 
            // receivedPanel
            // 
            this.receivedPanel.Controls.Add(this.cancelButton);
            this.receivedPanel.Controls.Add(this.addButton);
            this.receivedPanel.Controls.Add(this.newProfileTextBox);
            this.receivedPanel.Location = new System.Drawing.Point(9, 354);
            this.receivedPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.receivedPanel.Name = "receivedPanel";
            this.receivedPanel.Size = new System.Drawing.Size(191, 23);
            this.receivedPanel.TabIndex = 4;
            this.receivedPanel.Visible = false;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(168, 2);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(21, 18);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "×";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(142, 2);
            this.addButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(22, 18);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "↑";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // receiveTimer
            // 
            this.receiveTimer.Interval = 60000;
            this.receiveTimer.Tick += new System.EventHandler(this.receiveTimer_Tick);
            // 
            // IRTForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 380);
            this.Controls.Add(this.receivedPanel);
            this.Controls.Add(this.receiveButton);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.profileListBox);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "IRTForm";
            this.Text = "IRTransceiver";
            this.receivedPanel.ResumeLayout(false);
            this.receivedPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox profileListBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Button receiveButton;
        private System.Windows.Forms.TextBox newProfileTextBox;
        private System.Windows.Forms.Panel receivedPanel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Timer receiveTimer;
    }
}

