#include <IRremote.h>

#define IR_RECV_PIN 7
#define MV_SNSR_PIN 2
#define US_TRIG_PIN 10
#define US_ECHO_PIN 11

#define MODE_SEND 1
#define MODE_RCV 2

#define CMD_CHMODE_SEND 0x81
#define CMD_CHMODE_RCV 0x82
#define CMD_CAST_IR 0xF1
#define CMD_CATCH_IR 0xF2
#define CMD_HANDSHAKE 0x8F
#define CMD_MOVE_DET 0xE1
#define CMD_MOVE_UNDET 0xE2
#define CMD_READY 0xFF
#define CMD_HANDSHAKE_RES 0xF8
#define CMD_RESUME 0x11

IRsend irsend;
int recvPin = IR_RECV_PIN;
IRrecv irrecv(recvPin);
int mode = MODE_SEND;
//int dur_i = 0;
//double duration[4] = {-1};
bool detected = false;

void setup() {
  pinMode(13,OUTPUT);//debug LED
  pinMode(MV_SNSR_PIN,INPUT);
  //pinMode(US_ECHO_PIN,INPUT);
  //pinMode(US_TRIG_PIN,OUTPUT);
  Serial.begin(9600);   // Status message will be sent to PC at 9600 baud
  Serial.setTimeout(3000UL);
  irrecv.enableIRIn();
}

void loop() {
  int freq = 38; // 38kHz carrier frequency for the NEC protocol
  int cmd = Serial.read();
  if(cmd != -1){
    switch(cmd){
      case CMD_RESUME:
        digitalWrite(13,HIGH);
        while(Serial.read()!=CMD_HANDSHAKE){
          delay(50);
        }
        digitalWrite(13,LOW);
        Serial.write(CMD_HANDSHAKE_RES);
        delay(50);
        break;
      case CMD_CHMODE_SEND:
        mode = MODE_SEND;
        break;
      case CMD_CHMODE_RCV:
        irrecv.enableIRIn();  // Start the receiver
        mode = MODE_RCV;
        break;
      case CMD_CAST_IR:
        Serial.write(CMD_READY);
        byte buf[255] = {0};
        Serial.readBytes(buf,255);
        if(buf[0] != CMD_CAST_IR)
          break;
        if(mode == MODE_SEND){
          if(buf[1]==0){
            unsigned long data=((unsigned long)buf[4]<<24)|((unsigned long)buf[5]<<16)|((unsigned long)buf[6]<<8)|(unsigned long)buf[7];
            int nbits=buf[3];
            switch(buf[2]){
              case NEC:          irsend.sendNEC(data,nbits);           break;
              case SONY:         irsend.sendSony(data,nbits);          break;
              case RC5:          irsend.sendRC5(data,nbits);           break;
              case RC6:          irsend.sendRC6(data,nbits);           break;
              case DISH:         irsend.sendDISH(data,nbits);          break;
              case SHARP:        break;
              case JVC:          irsend.sendJVC(data,nbits,false);     break;
              case SANYO:        break;
              case MITSUBISHI:   break;
              case SAMSUNG:      irsend.sendSAMSUNG(data,nbits);       break;
              case LG:           irsend.sendLG(data,nbits);            break;
              case WHYNTER:      irsend.sendWhynter(data,nbits);       break;
              case AIWA_RC_T501: break;
              case PANASONIC:    break;
              case DENON:        irsend.sendDenon(data,nbits);         break;
            }
          }
          else{
            unsigned int irSignal[128];
          int i;
          for(i=0;i<buf[1];i++){
            irSignal[i]=makeWord(buf[i*2+3],buf[i*2+2]);
          }
          irsend.sendRaw(irSignal,buf[1],freq);
          }
        }
        break;
    }
  }
  
  if(mode == MODE_RCV){
    decode_results results;
    if(irrecv.decode(&results)){
      Serial.write(CMD_CATCH_IR);
      unsigned char buf[255] = {0};
      buf[0] = CMD_CATCH_IR;
      if(results.decode_type!=UNKNOWN){
        if(results.value==0xFFFFFFFF){
          irrecv.resume();
          delay(100);
          return;
        }
        buf[1]=0;
        buf[2]=results.decode_type;
        buf[3]=(unsigned char)(results.bits);
        buf[4]=(unsigned char)(results.value>>24);
        buf[5]=(unsigned char)(results.value>>16);
        buf[6]=(unsigned char)(results.value>>8);
        buf[7]=(unsigned char)(results.value);
      }
      else{
        buf[1] = results.rawlen-1;
        int i;
        for(i=1;i<results.rawlen;i++){
          buf[i*2]=lowByte(results.rawbuf[i] * USECPERTICK);
          buf[i*2+1]=highByte(results.rawbuf[i] * USECPERTICK);
        }
      }
      byte buf2[1] = {0};
      Serial.readBytes(buf2,1);
      if(buf2[0]==CMD_READY)
        Serial.write(buf,255);
      delay(50);
      irrecv.resume(); 
    }
  }

  if(detected && !digitalRead(MV_SNSR_PIN)){
    Serial.write(CMD_MOVE_UNDET);
    delay(50);
    detected = false;
  }
  if(!detected && digitalRead(MV_SNSR_PIN)){
    Serial.write(CMD_MOVE_DET);
    delay(50);
    detected = true;
  }
  
  delay(50);/*
  digitalWrite(US_TRIG_PIN, LOW); 
  delayMicroseconds(2); 
  digitalWrite( US_TRIG_PIN, HIGH ); //超音波を出力
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG_PIN, LOW );
  duration[dur_i] = pulseIn( US_ECHO_PIN, HIGH,60000 ); //センサからの入力
  if(duration[dur_i+1%4] > 0 && abs(duration[(dur_i+1)%4] - duration[dur_i]) > 500){
    unsigned char buf[255];
    buf[0]=CMD_MOVE_DET;
    digitalWrite(13,HIGH);
    Serial.write(buf,255);
    delay(50);
  }
  digitalWrite(13,LOW);
  dur_i=dur_i+1%4;*/
}
