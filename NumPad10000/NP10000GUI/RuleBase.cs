﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using NAudio.Wave;

namespace NP10000
{
	public class RuleBase
	{
        
		public ArduinoTransceiver arduinoTransceiver = new ArduinoTransceiver();
        public NP10000.NP10000Form form;
        public static readonly string ProfileDir = Path.Combine(Application.StartupPath, "profiles");
        private WaveOut player;
		private System.Threading.Thread threadObject;
		private ConcurrentDictionary<String, InvokeTime> invokeList = new ConcurrentDictionary<String, InvokeTime>();
        private bool exit = false;

		public RuleBase()
		{
            form = NP10000.Program.form;
			threadObject = new System.Threading.Thread(new System.Threading.ThreadStart(thread));
			threadObject.Start();
		}

        public void Dispose()
        {
            exit = true;
            System.Threading.Thread.Sleep(1000);
            if(threadObject.IsAlive)
                threadObject.Abort();
            arduinoTransceiver.Dispose();
        }

        private void thread()
		{
			while (!exit)
			{
				List<String> doneList = new List<String>();
				lock (invokeList)
				{
					foreach (var invoke in invokeList)
					{
						if (DateTime.Now > invoke.Value.TimeNext)
						{
							invoke.Value.Function();
                            if (invoke.Value.Loop)
                            {
                                if (invoke.Value.DelaySecond == 0)
                                    invoke.Value.TimeNext = invoke.Value.TimeNext.AddDays(1);
                                else
                                    invoke.Value.TimeNext = DateTime.Now.AddSeconds(invoke.Value.DelaySecond);
                            }
                            else
                                doneList.Add(invoke.Key);
						}
					}
				}
				foreach (var done in doneList)
				{
					InvokeTime invokeTime;
					while (!invokeList.TryRemove(done, out invokeTime))
						System.Threading.Thread.Sleep(150);
				}
				System.Threading.Thread.Sleep(200);
			}
		}

        public void PlaySound(string waveFile)
        {
            //再生されているときは止める
            if (player.PlaybackState == PlaybackState.Playing)
                StopSound();
            AudioFileReader reader = new AudioFileReader(waveFile);
            
            player.Init(reader);
            player.Play();

        }

        public void PlaySE(string waveFile)
        {
            AudioFileReader reader = new AudioFileReader(waveFile);

            WaveOut waveOut = new WaveOut();
            waveOut.Init(reader);
            waveOut.Play();
        }

        public void StopSound()
        {
            player.Stop();
            player.Dispose();
        }

        public void LoopInvoke(long delaySec, Func<int> f)
		{
			string key = Guid.NewGuid().ToString();
            LoopInvokeLabel(delaySec, f, key);
		}

		public void LoopInvokeLabel(long delaySec, Func<int> f, string label)
		{
			InvokeTime invokeTime;
			if (invokeList.ContainsKey(label))
				while (invokeList.ContainsKey(label) && !invokeList.TryRemove(label, out invokeTime))
					System.Threading.Thread.Sleep(150);
			invokeTime = new InvokeTime()
			{
				TimeNext = DateTime.Now,
				DelaySecond = delaySec,
				Function = f,
				Loop = true
			};
			while (!invokeList.TryAdd(label, invokeTime))
				System.Threading.Thread.Sleep(150);
		}

		public void DelayInvoke(long delaySec, Func<int> f)
		{
			string key = Guid.NewGuid().ToString();
            DelayInvokeLabel(delaySec, f, key);
		}

		public void DelayInvokeLabel(long delaySec, Func<int> f, string label)
		{
			InvokeTime invokeTime;
			if (invokeList.ContainsKey(label))
				while (invokeList.ContainsKey(label) && !invokeList.TryRemove(label, out invokeTime))
					System.Threading.Thread.Sleep(150);
			invokeTime = new InvokeTime()
			{
				TimeNext = DateTime.Now.AddSeconds(delaySec),
				Function = f,
				Loop = false
			};
			while (!invokeList.TryAdd(label, invokeTime))
				System.Threading.Thread.Sleep(150);
		}

        public void TimeInvoke(DateTime dt, Func<int> f, bool loop)
        {
            string key = Guid.NewGuid().ToString();
            TimeInvokeLabel(dt, f, loop, key);
        }

        public void TimeInvokeLabel(DateTime dt, Func<int> f, bool loop, string label)
        {
            InvokeTime invokeTime;
            if (invokeList.ContainsKey(label))
                while (invokeList.ContainsKey(label) && !invokeList.TryRemove(label, out invokeTime))
                    System.Threading.Thread.Sleep(150);
            var now = DateTime.Now;
            var newTime = new DateTime(now.Year, now.Month, now.Day, dt.Hour, dt.Minute, dt.Second);
            invokeTime = new InvokeTime()
            {
                TimeNext = now < newTime ? newTime : newTime.AddDays(1),
                Function = f,
                Loop = loop
            };
            while (!invokeList.TryAdd(label, invokeTime))
                System.Threading.Thread.Sleep(150);
        }

        public void RemoveInvoke(string label)
		{
			InvokeTime invokeTime;
			if (invokeList.ContainsKey(label))
				while (invokeList.ContainsKey(label) && !invokeList.TryRemove(label, out invokeTime))
					System.Threading.Thread.Sleep(150);
		}

		public class InvokeTime
		{
			public DateTime TimeNext;
			public long DelaySecond;
			public Func<int> Function;
			public bool Loop;
		}

		public class TriggerListener
		{
			public string TriggerName;
			public Func<int> Function;
		}

		public enum PROTOCOL { TRIGGER }
		public enum PRESSKEY { DOWN, UP, UPORLONG, LONG }

		public class ButtonListener
		{
			public Keys Key;
			public PRESSKEY KeyMode;
			public Func<int> Function;
			public long PressTimeStamp;
		}

		public static class Extends
        {
            public static bool InDaylight()
            {
                int day = DateTime.Now.DayOfYear;
                double time = DateTime.Now.Hour + DateTime.Now.Minute / 60.0d;
                return SunRiseTime(day) + 0.5 < time && SunSetTime(day) - 0.5 > time;
            }

            public static void SaveArrayValues(string fileName, int[] array)
            {
                string[] str = new string[array.Length];
                int i = 0;
                foreach (var arr in array)
                {
                    str[i++] = arr.ToString();
                }
                File.WriteAllLines(fileName, str, System.Text.Encoding.UTF8);
            }

            public static int[] LoadArrayValues(string fileName)
            {
                string[] str = File.ReadAllLines(fileName, System.Text.Encoding.UTF8);
                int[] array = new int[str.Length];
                int i = 0;
                foreach (var s in str)
                {
                    array[i++] = int.Parse(s);
                }
                return array;
            }

            public static void SendMagicPacket(int port, IPAddress broad, byte[] macAddress)
            {
                MemoryStream stream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(stream);
                for (int i = 0; i < 6; i++)
                {
                    writer.Write((byte)0xff);
                }
                for (int i = 0; i < 16; i++)
                {
                    writer.Write(macAddress);
                }

                UdpClient client = new UdpClient();
                client.EnableBroadcast = true;
                client.Send(stream.ToArray(), (int)stream.Position, new IPEndPoint(broad, port));
            }

            private static double SunRiseTime(int n)
            {   // 日の出時刻を求める関数
                const double x = 144.365359;
                const double y = 43.015080 * Math.PI / 180.0d;

                double d, e, t;
                d = dCalc(n);                                // 太陽赤緯を求める
                e = eCalc(n);                                // 均時差を求める
                                                             // 太陽の時角幅を求める（視半径、大気差などを補正 (-0.899度)　）
                t = (Math.Acos((Math.Sin(-0.899 * Math.PI / 180.0d) - Math.Sin(d) * Math.Sin(y)) / (Math.Cos(d) * Math.Cos(y)))) * 180 / Math.PI;
                return (-t + 180.0 - x + 135.0) / 15.0 - e; // 日の出時刻を返す
            }

            private static double SunSetTime(int n)
            {    // 日の入り時刻を求める関数
                const double x = 144.365359;
                const double y = 43.015080 * Math.PI / 180.0d;
                double d, e, t;
                d = dCalc(n);                                // 太陽赤緯を求める
                e = eCalc(n);                                // 均時差を求める
                                                             // 太陽の時角幅を求める（視半径、大気差などを補正 (-0.899度)　）
                t = (Math.Acos((Math.Sin(-0.899 * Math.PI / 180.0d) - Math.Sin(d) * Math.Sin(y)) / (Math.Cos(d) * Math.Cos(y)))) * 180 / Math.PI;
                return (t + 180.0 - x + 135.0) / 15.0 - e;  // 日の入り時刻を返す
            }

            private static double dCalc(int n)
            {
                double d, w;
                w = (n + 0.5) * 2 * Math.PI / 365;              // 日付をラジアンに変換
                d = +0.33281
                    - 22.984 * Math.Cos(w) - 0.34990 * Math.Cos(2 * w) - 0.13980 * Math.Cos(3 * w)
                    + 3.7872 * Math.Sin(w) + 0.03250 * Math.Sin(2 * w) + 0.07187 * Math.Sin(3 * w);
                return d * Math.PI / 180.0d;
            }

            private static double eCalc(int d)
            {                           // 近似式で均時差を求める
                double e, w;
                w = (d + 0.5) * 2 * Math.PI / 365;              // 日付をラジアンに換算
                e = +0.0072 * Math.Cos(w) - 0.0528 * Math.Cos(2 * w) - 0.0012 * Math.Cos(3 * w)
                    - 0.1229 * Math.Sin(w) - 0.1565 * Math.Sin(2 * w) - 0.0041 * Math.Sin(3 * w);
                return e;                                    // 均一時差を返す（単位は時）
            }
        }
	}
}