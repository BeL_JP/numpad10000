﻿//190213debug
using System;
using System.IO;
using System.Net;

private int[] volumeTimeline = { 0, 0, 0, -1, -2, -3,
                                    -2, -1, 1, 3, 4, 4,
                                    4, 4, 4, 4, 3, 2,
                                    2, 2, 2, 1, 1, 1 };
private int[] AFKTimeout = { 300, 300, 300, 200, 200, 150,
                                    200, 200, 200, 300, 300, 300,
                                    300, 300, 300, 300, 300, 300,
                                    300, 300, 300, 300, 300, 300 };
private int currentVolume = 0;
private int correctVolume = 0;
private bool speakerPower = false;
private bool inDay = true;
private bool inMid = false;
private bool waitExit = false;
private bool sleeping = false;
private bool cleaned = true;
private int ownerAliveLevel = 0;
private int currentHDMI = 1;
private System.Media.SoundPlayer tSPlayer = new System.Media.SoundPlayer(Path.Combine(ProfileDir, "bell04.wav"));

private void KeyMap()
{
    form.AddButtonListener(System.Windows.Forms.Keys.D, PRESSKEY.UP, Tester);
    form.AddButtonListener(System.Windows.Forms.Keys.NumPad1, PRESSKEY.UP, ToggleHDMIDevice);
    form.AddButtonListener(System.Windows.Forms.Keys.NumPad0, PRESSKEY.UP, SleepMode);
    form.AddButtonListener(System.Windows.Forms.Keys.NumPad9, PRESSKEY.UP, RestartApp);
    form.AddButtonListener(System.Windows.Forms.Keys.Decimal, PRESSKEY.UP, SpeakerPowerToggle);
    form.AddButtonListener(System.Windows.Forms.Keys.Add, PRESSKEY.UP, VolumeUp);
    form.AddButtonListener(System.Windows.Forms.Keys.Subtract, PRESSKEY.UP, VolumeDown);
    form.AddButtonListener(System.Windows.Forms.Keys.NumPad7, PRESSKEY.UP, CleanReservation);
}

private bool InMidnight(){
    return DateTime.Now.Hour < 7 && !inDay;
}

private void Form_AnyKeyPressed(object sender, System.Windows.Forms.KeyEventArgs e)
{
/*
    if (sleeping)
    {
        if(InMidnight())
            IRSender.LightMemOn(arduinoTransceiver);
        else
            IRSender.LightOn(arduinoTransceiver);
        IRSender.PowerSwitchOn(arduinoTransceiver);
        sleeping = false;
        correctVolume = 0;
        TimerVolume();

        sleeping = false;
    }*/
}

private int SpeakerPowerToggle()
{
    IRSender.SpeakerPower(arduinoTransceiver);
    return 0;
}

private int VolumeUp()
{
    volumeTimeline[DateTime.Now.Hour]++;
    TimerVolume();
    return 0;
}

private int VolumeDown()
{
    volumeTimeline[DateTime.Now.Hour]--;
    TimerVolume();
    return 0;
}

private int RestartApp()
{
    form.RestartApplication();
    return 0;
}

private int CleanReservation()
{
    cleaned = false;
    return 0;
}

private int SleepMode()
{
    if(sleeping){
        if (!inDay)
        {
            if (InMidnight())
                IRSender.LightMemOn(arduinoTransceiver);
            else
                IRSender.LightOn(arduinoTransceiver);
        }
        IRSender.PowerSwitchOn(arduinoTransceiver);
        sleeping = false;
        correctVolume = 0;
        TimerVolume();
        sleeping = false;
    }
    else{
        IRSender.LightOff(arduinoTransceiver);
        IRSender.PowerSwitchOff(arduinoTransceiver);
        correctVolume = -4;
        TimerVolume();
        sleeping = true;
    }
    return 0;
}

private int ToggleHDMIDevice()
{
    if (currentHDMI == 1)
    {
        IRSender.SetHDMI2(arduinoTransceiver);
        currentHDMI = 2;
    }
    else if (currentHDMI == 2)
    {
        IRSender.SetHDMI1(arduinoTransceiver);
        currentHDMI = 1;
    }
    return 0;
}

private void MoveDetected(object sender, EventArgs e)
{
    if(sleeping)
        return;
    RemoveInvoke("AFKNOTIFY");
    RemoveInvoke("AFK");
    if (ownerAliveLevel == 0)
    {
        OwnerAlive();
    }
    else if (ownerAliveLevel == 1)
    {
        AFKTimeout[DateTime.Now.Hour] += 20;
        IRSender.PowerSwitchOn(arduinoTransceiver);
        correctVolume = 0;
        TimerVolume();
        ownerAliveLevel = 2;
    }
}

private void MoveUndetected(object sender, EventArgs e)
{
    DelayInvokeLabel(AFKTimeout[DateTime.Now.Hour] - 30, AFKNotify, "AFKNOTIFY");
    DelayInvokeLabel(AFKTimeout[DateTime.Now.Hour], OwnerLeft, "AFK");
}

private int OwnerAlive()
{
    if(sleeping)
        return 0;
    if (!inDay){
        if(InMidnight())
            IRSender.LightMemOn(arduinoTransceiver);
        else
            IRSender.LightOn(arduinoTransceiver);
    }
    Extends.SendMagicPacket(9, IPAddress.Parse("192.168.1.255"), new byte[] { 0x70, 0x85, 0xC2, 0x80, 0x55, 0xEC });
    if(!speakerPower){
        speakerPower = false;
        IRSender.SpeakerPower(arduinoTransceiver);
    }
    IRSender.PowerSwitchOn(arduinoTransceiver);
    speakerPower = true;
    ownerAliveLevel = 2;
    return 0;
}

private int AFKNotify()
{
    if(sleeping)
        return 0;
    IRSender.PowerSwitchOff(arduinoTransceiver);
    correctVolume = -4;
    TimerVolume();
    ownerAliveLevel = 1;
    return 0;
}

private int OwnerLeft()
{
    if(sleeping)
        return 0;
    form.SendTriggerAll("SHUTDOWN");
    if (!inDay)
        IRSender.LightOff(arduinoTransceiver);
    if(speakerPower){
        speakerPower = false;
        IRSender.SpeakerPower(arduinoTransceiver);
    }
    IRSender.PowerSwitchOff(arduinoTransceiver);
    ownerAliveLevel = 0;
    return 0;
}

private int TimerVolume()
{
    SetVolume(volumeTimeline[DateTime.Now.Hour] + correctVolume);
    return 0;
}

private void SetTimeSignal(){
    var now = DateTime.Now;
    var dt = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0);
    Console.WriteLine(dt.AddHours(1));
    TimeInvoke(dt.AddHours(1),TimeSignal,false);
}

private int TimeSignal(){
    if(!InMidnight() && !sleeping && ownerAliveLevel != 0){
        int count = DateTime.Now.Hour % 12;
        count = count == 0 ? 12 : count;
        for(int i = 0;i < count;i++){
            tSPlayer.Play();
            System.Threading.Thread.Sleep(1000);
        }
    }
    SetTimeSignal();
    return 0;
}

private int TimerLight()
{
    if (inDay && !Extends.InDaylight() && ownerAliveLevel != 0 && !sleeping)
    {
        IRSender.LightOn(arduinoTransceiver);
    }
    else if (!inMid && InMidnight() && ownerAliveLevel != 0 && !sleeping)
    {
        IRSender.LightMemOn(arduinoTransceiver);
    }
    else if (!inDay && Extends.InDaylight())
    {
        cleaned = false;
        IRSender.LightOff(arduinoTransceiver);
    }
    inDay = Extends.InDaylight();
    inMid = InMidnight();
    return 0;
}

private void SetVolume(int vol)
{
    if (speakerPower)
    {
        while (vol != currentVolume)
        {
            System.Threading.Thread.Sleep(500);
            if (vol > currentVolume)
            {
                IRSender.VolumeUp(arduinoTransceiver);
                currentVolume++;
            }
            else if (vol < currentVolume)
            {
                IRSender.VolumeDown(arduinoTransceiver);
                currentVolume--;
            }
        }
    }
}

private int SaveProfile()
{
    Extends.SaveArrayValues(Path.Combine(RuleBase.ProfileDir, "volumeTimeline.txt"), volumeTimeline);
    Extends.SaveArrayValues(Path.Combine(RuleBase.ProfileDir, "AFKTimeout.txt"), AFKTimeout);
    return 0;
}

private int Cleaner()
{
    if (!cleaned && inDay && !sleeping && ownerAliveLevel == 0)
    {
        IRSender.Clean(arduinoTransceiver);
        cleaned = true;
    }
    return 0;
}

private int Tester()
{
    IRSender.SpeakerPower(arduinoTransceiver);
    IRSender.LightOff(arduinoTransceiver);
    IRSender.PowerSwitchOff(arduinoTransceiver);
    IRSender.SetHDMI2(arduinoTransceiver);
    Console.WriteLine("Debug: Turn off all.");
    System.Threading.Thread.Sleep(5000);
    IRSender.SpeakerPower(arduinoTransceiver);
    IRSender.LightOn(arduinoTransceiver);
    IRSender.PowerSwitchOn(arduinoTransceiver);
    IRSender.SetHDMI1(arduinoTransceiver);
    Console.WriteLine("Debug: Turn on all.");
    return 0;
}

public static class IRSender
{
    public static void VolumeUp(ArduinoTransceiver at)
    {
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "SP_VOL_UP.irb"));
    }

    public static void VolumeDown(ArduinoTransceiver at)
    {
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "SP_VOL_DOWN.irb"));
    }

    public static void SpeakerPower(ArduinoTransceiver at)
    {
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "SP_PW.irb"));
    }

    public static void LightOn(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_OFF.irb"));
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_MEM_ON.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_ON.irb"));
    }

    public static void LightOff(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_MEM_ON.irb"));
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_ON.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_OFF.irb"));
    }

    public static void LightMemOn(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_OFF.irb"));
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_ON.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "LIGHT_MEM_ON.irb"));
    }

    public static void PowerSwitchOn(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "PWSW_OFF.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "PWSW_ON.irb"));
    }

    public static void PowerSwitchOff(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "PWSW_ON.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "PWSW_OFF.irb"));
    }

    public static void SetHDMI1(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "HDMI_2.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "HDMI_1.irb"));
    }

    public static void SetHDMI2(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "HDMI_1.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "HDMI_2.irb"));
    }

    public static void Clean(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN_END.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN.irb"));
        System.Threading.Thread.Sleep(1000);
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN.irb"));
    }

    public static void CleanEnd(ArduinoTransceiver at)
    {
        at.CancelCastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN.irb"));
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN_END.irb"));
        System.Threading.Thread.Sleep(1000);
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN_END.irb"));
        System.Threading.Thread.Sleep(1000);
        at.CastIR(Path.Combine(RuleBase.ProfileDir, "CLEAN_END.irb"));
    }

}
if(File.Exists(Path.Combine(RuleBase.ProfileDir, "volumeTimeline.txt")))
    volumeTimeline = Extends.LoadArrayValues(Path.Combine(RuleBase.ProfileDir, "volumeTimeline.txt"));
if (File.Exists(Path.Combine(RuleBase.ProfileDir, "AFKTimeout.txt")))
AFKTimeout = Extends.LoadArrayValues(Path.Combine(RuleBase.ProfileDir, "AFKTimeout.txt"));
tSPlayer.Load();
KeyMap();
inDay = Extends.InDaylight();
inMid = InMidnight();
currentVolume = volumeTimeline[DateTime.Now.Hour];
LoopInvoke(30, TimerVolume);
LoopInvoke(60, TimerLight);
LoopInvoke(60, SaveProfile);
LoopInvoke(90, Cleaner);
arduinoTransceiver.MoveDetected += MoveDetected;
arduinoTransceiver.MoveDetected += MoveUndetected;
form.AnyKeyPressed += Form_AnyKeyPressed;
SetTimeSignal();
Console.WriteLine("Rule loaded");
