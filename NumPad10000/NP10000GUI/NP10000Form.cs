﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using takuya.NetTool;
using static NP10000.RuleBase;
using System.IO;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System.Reflection;
using System.Runtime.InteropServices;

namespace NP10000
{
    public partial class NP10000Form : Form
    {
        public NetServer Net = new NetServer(25555, "NP10000");
        public event EventHandler<KeyEventArgs> AnyKeyPressed;

        private List<string> clients = new List<string>();
        private int activeClientId = -1;
        private bool timeSec = false;
        private List<TriggerListener> triggerListeners = new List<TriggerListener>();
        private List<ButtonListener> buttonListeners = new List<ButtonListener>();
        private const int longPressTime = 3000;
        private bool debug = false;
        private RuleBase rule;

        [DllImport("user32.dll")]
        private extern static int GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);

        [DllImport("user32.dll")]
        private extern static IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private extern static bool AttachThreadInput(int idAttach, int idAttachTo, bool fAttach);

        public NP10000Form()
        {         
            Net.SetDataReceiver((int)PROTOCOL.TRIGGER, triggerReceiver);
            if (!debug)
            {
                this.StartPosition = FormStartPosition.Manual;
                this.Location = new Point(0, 0);
            }
            InitializeComponent();
            if (!debug)
            {
                this.Size = Screen.PrimaryScreen.Bounds.Size;
                Cursor.Hide();
            }
            Net.Start();
        }

        private void ForceActivate()
        {
            int foreground = GetWindowThreadProcessId(GetForegroundWindow(), IntPtr.Zero);
            int me = System.Threading.Thread.CurrentThread.ManagedThreadId;
            if (foreground == me) return;

            AttachThreadInput(me, foreground, true);
            this.Activate();
            AttachThreadInput(me, foreground, false);
        }

        public void RestartApplication()
        {
            //System.Diagnostics.Process.Start(Application.ExecutablePath);
            rule.Dispose();
            //Program.mutex.ReleaseMutex();
            //Program.mutex.Close();
            Application.Exit();
        }

        public void AddButtonListener(Keys key, PRESSKEY keymode, Func<int> func)
        {
            buttonListeners.Add(new ButtonListener() { Key = key, KeyMode = keymode, Function = func, PressTimeStamp = 0 });
        }

        public void SendTrigger(string triggerName)
        {
            Net.Write((int)PROTOCOL.TRIGGER, triggerName, getActiveClient());
        }

        public void SendTriggerAll(string triggerName)
        {
            Net.WriteBroadcast((int)PROTOCOL.TRIGGER, triggerName);
        }

        public void AddTriggerListener(string triggerName, Func<int> func)
        {
            triggerListeners.Add(new TriggerListener() { TriggerName = triggerName, Function = func });
        }

        public string getActiveClient()
        {
            return Net.cls[activeClientId].Name;
        }

        private void triggerReceiver(object sender, ReadEventArgs e)
        {
            foreach (var tl in triggerListeners)
            {
                if (e.Text == tl.TriggerName)
                    tl.Function();
            }
        }

        private void dateTimeTimer_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            timeLabel.Text = dt.Hour + ":" + string.Format("{0:D2}", dt.Minute);
            timeSec = !timeSec;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (AnyKeyPressed != null)
                AnyKeyPressed(sender, e);
            foreach (var bl in buttonListeners)
            {
                if (bl.Key == e.KeyData)
                {
                    bl.PressTimeStamp = DateTime.Now.Ticks;
                    if (bl.KeyMode == PRESSKEY.DOWN)
                        bl.Function();
                }
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            foreach (var bl in buttonListeners)
            {
                if (bl.Key == e.KeyData)
                {
                    if (DateTime.Now.Ticks - bl.PressTimeStamp > longPressTime * 10000)
                    {
                        if (bl.KeyMode == PRESSKEY.LONG)
                            bl.Function();
                        else if (bl.KeyMode == PRESSKEY.UPORLONG)
                        {
                            bl.Function();
                        }
                    }
                    else if (bl.KeyMode == PRESSKEY.UP)
                        bl.Function();
                }
            }
        }

        private void NP10000Form_Load(object sender, EventArgs e)
        {
        }

        private void LoadScript(string fileName)
        {
            string code;
            using(var reader = new StreamReader(fileName))
            {
                code = reader.ReadToEnd();
            }
            ScriptOptions options = ScriptOptions.Default
                                                 .WithReferences(Assembly.GetAssembly(typeof(RuleBase)), Assembly.GetAssembly(typeof(ArduinoTransceiver)), Assembly.GetEntryAssembly(), Assembly.GetAssembly(typeof(System.Net.Dns)))
                                                 .WithImports("NP10000");
            var script = CSharpScript.Create(code,options,typeof(RuleBase));
            script.RunAsync(rule = new RuleBase());
        }

        private void NP10000Form_Shown(object sender, EventArgs e)
        {
            //ForceActivate();//windows
            timeLabel.Focus();
            LoadScript(Path.Combine(ProfileDir, "MyRule.csx"));
            dateTimeTimer.Start();
        }
    }
}
