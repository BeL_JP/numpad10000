﻿using System;
using System.Windows.Forms;

namespace NP10000
{
    static class Program
    {
        public static NP10000Form form;
        public static string mutexName = "NP10000";
        public static bool createdNew;
        public static System.Threading.Mutex mutex;
        public static int startRetry = 10;
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*while(!createdNew)
            {
                mutex.Close();
                if (startRetry <= 0)
                    return;
                System.Threading.Thread.Sleep(1000);
                mutex = new System.Threading.Mutex(true, mutexName, out createdNew);
                startRetry--;
            }*/
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(form = new NP10000Form());
        }
    }
}
